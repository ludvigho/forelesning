# Python Calendar Example

# import calendar module
import calendar

# ask of month and year
year = input("year: ")
month = input("month: ")

# Displaying the Python calendar
print(calendar.month(year, month))

# Hvorfor kræsjer dette? Kan vi gjøre det bedre med å skrive siste linje slik?
#print(calendar.month(int(year), int(month)))
